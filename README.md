# Project 2 by Team 22 in p2t222 repo
MVP- Finance management web app, using RESTful API model. Home page with menu with stock search, configurable stock info charts with realtime and historical data. Stock search inclusive for name or ticker symbol. Real estate investment page with multiple tools for property buyers, home or rental. Create portfolios with compare function for analysis of risk and growth between different portfolio options, like portfolio investment balance by related industries (ex: tech, retail, etc). Created for use with remote hosted site and server provided by TechWise Software Engineer training program. In the near future, if and when a new host is acquired, app will be available for public use. 

## Visuals
- [Menu demo of Home, About, Services pages](https://gitlab.com/tw-p2/p2t22/-/blob/main/app%20demos/demo_menu_pages.gif)
- [Real eastate underwriting tools demo](https://gitlab.com/tw-p2/p2t22/-/blob/main/app%20demos/demo_underwriting_tools.gif)
- [Trading analysis demo](https://gitlab.com/tw-p2/p2t22/-/blob/main/app%20demos/demo_trade_analysis.gif)
- [User account creation frontend and backend demo](https://gitlab.com/tw-p2/p2t22/-/blob/main/ap%20demos/demo_user_acct_entry_added_SQL_table.gif)

## Authors and acknowledgment
### Authors:
- Anis Meah - Frontend Developer
- Maimouna Diallo - Fullstack Developer
- April Gilmore - Backend Developer
- Kanchana Iyer - Fullstack Developer
### Authors wish to thank our mentors for their support and guidance:  
- Morgan Henning
- Nicholas Groesch

### Our grateful acknowledgment of additional support from:
- TechWise Software Engineer training program's instructors, administrators, and fellow students

## Project's frontend repo:
https://gitlab.com/ameah1102/project-2.git

## Project's first hosted site (no longer available):
https://portfolios.talentsprint.com/team22/
