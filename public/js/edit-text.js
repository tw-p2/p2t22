const paragraph = document.getElementById("edit");
const edit_button = document.getElementById("edit-button");
const end_button = document.getElementById("end-editing");

edit_button.addEventListener("click", function() {
  paragraph.contentEditable = true;
  paragraph.style.border = "thin solid";
} );

end_button.addEventListener("click", function() {
  paragraph.contentEditable = false;
  paragraph.style.border = "thin solid #fff";
} )