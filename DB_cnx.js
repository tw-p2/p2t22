require("dotenv").config();
const mysql = require('mysql2');

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
});
console.log("host + name: " + process.env.DB_HOST + " " + process.env.DB_NAME);
module.exports = connection;
// Tables in team22 db:
//** test DB: 2 cols - username, password */
//** demo & users DB: 5 cols- id(key, autoincr), username, pswd, email, date_made*/


//*** April's local db*/
// const connection = mysql.createConnection({
//     host: 'localhost',
//     user: process.env.DB_USER_ICI,
//     password: process.env.DB_PASSWORD_ICI,
//     database: 'slaw',
// });

