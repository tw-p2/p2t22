function noAcct() {
  if (document.cookie == "" || document.cookie == null)
    window.location.replace('https://portfolios.talentsprint.com/team22/login.html');
}

if  (document.cookie != "" && document.cookie != null) {
  var btn = document.querySelector("#chgBtn span");
  btn.innerHTML = "Logout";
} else if (document.cookie == "" || document.cookie == null) {
  var btn = document.querySelector("#chgBtn span");
  btn.innerHTML = "Signin";
}
