console.log("-you rang PROFILE?")

const update = document.querySelector("#clk_update");
update.addEventListener("click", validateData);

var aboutData = document.getElementsByClassName("input");
console.log("data abt: " + aboutData)
const fullNameInput = aboutData.item(0);
console.log("fullNameInput: " + fullNameInput.value);
const emailInput = aboutData.item(1);
const phoneInput = aboutData.item(2);
const urlLinkedInInput = aboutData.item(3);
const streetInput = aboutData.item(4);
const cityInput = aboutData.item(5);
const stateInput = aboutData.item(6);
const zipInput = aboutData.item(7);


//console.log("emailInput: " + emailInput.value);
//console.log("passwordInput: " + passwordInput.value);

function validateData(event) {
  console.log("-you rang validData PROFILE?")
  event.preventDefault();
  if (noEmptyFields()) {
      addProfile();
  } else {
      console.log("-data not valid");
  }
}



function isNotEmpty(input) {
  if (input.value == "") {
    input.value = "_";
  }
  return input.value !== "";
}

function noEmptyFields() {
  return formInputs.every(isNotEmpty);
}

const formInputs = [
  fullNameInput,
  emailInput,
  phoneInput,
  urlLinkedInInput,
  streetInput,
  cityInput,
  stateInput,
  zipInput,
];


var redirectLogin = false;

async function addProfile() {
    console.log("-you rang signupUser?")
    try {
        const theData = {
          fullName: fullNameInput.value,
          email: emailInput.value,
          phone: phoneInput.value,
          urlLinked: urlLinkedInInput.value,
          street: streetInput.value,
          city: cityInput.value,
          state: stateInput.value,
          zip: zipInput.value,
        };
        console.log(theData);
        const response = await fetch("/team22/users/profile", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(theData),
        });
        const data = await response.json();
      console.log("Success:", data);
      gotoAbout();
    } catch (error) {
        console.error("Error:", error);
    }
}

function gotoAbout() {
  window.location.replace('https://portfolios.talentsprint.com/team22/about.html');
}

// function getFormLoginInfo() {
//     const loginForm = document.forms.formLogin;
//     const properties = [
//         "elements",
//         "length",
//         "name",
//     ];
//     const info = properties
//         .map((property) => `${property}: ${f[property]}`)
//         .join("\n");
//     console.log("info properties: " + info);  
//     const inputs = [].map.call(
//         document.querySelectorAll(".input"),
//         function (el) {
//             return new loginForm(el);
//         }
//     );
//     console.log("inputs - queryALL: " + inputs); 
// }