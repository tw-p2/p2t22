let login = document.querySelector("#clk_signup");

const loginForm = document.forms.formSignup;
const usernameInput = loginForm[0];
const emailInput = loginForm[1];
const passwordInput = loginForm[2];

console.log("usernameInput: " + usernameInput.value);
console.log("emailInput: " + emailInput.value);
console.log("passwordInput: " + passwordInput.value);

function validateData(event) {
    console.log("-you rang validData SIGNUP")
    event.preventDefault();
    if (noEmptyFields()) {
        signupUser();
    } else {
        console.log("-data not valid");
    }
}

login.addEventListener("click", validateData);

function isNotEmpty(input) {
    return input.value !== "";
}

const formInputs = [
  usernameInput,
  emailInput,
  passwordInput,
];

function noEmptyFields() {
    return formInputs.every(isNotEmpty);
}

var redirectLogin = false;

async function signupUser() {
    console.log("-you rang signupUser?")
    try {
        const theData = {
          username: usernameInput.value,
          email: emailInput.value,
          password: passwordInput.value,
        };
        console.log(theData);
        const response = await fetch("/team22/users", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(theData),
        });
        const data = await response.json();
      console.log("Success:", data);
      gotoLogin();
    } catch (error) {
        console.error("Error:", error);
    }
}

function gotoLogin() {
  window.location.replace('https://portfolios.talentsprint.com/team22/login.html');
}

// function getFormLoginInfo() {
//     const loginForm = document.forms.formLogin;
//     const properties = [
//         "elements",
//         "length",
//         "name",
//     ];
//     const info = properties
//         .map((property) => `${property}: ${f[property]}`)
//         .join("\n");
//     console.log("info properties: " + info);  
//     const inputs = [].map.call(
//         document.querySelectorAll(".input"),
//         function (el) {
//             return new loginForm(el);
//         }
//     );
//     console.log("inputs - queryALL: " + inputs); 
// }