const express = require("express");
const session = require("express-session");
const passport = require("passport");
const bodyParser = require("body-parser");
const morgan = require("morgan");
var request = require("request");
const path = require("path");
const { resolve } = require("path");
const db = require("./DB_cnx");
require("dotenv").config();

const usersRoute = require("./routes/users");
const local = require("./strategies/local");
// const viewRoute = require("./routes/views");
// const profileRoute = require('./routes/profile');
const authRoute = require("./routes/auth");

const port = 8121;
if (process.env.MODE === "dev") {
  port = 3000;
}

const app = express();
const store = new session.MemoryStore();

// configs

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  session({
    secret: "keybrd cat",
    cookie: { maxAge: 800000 },
    // resave: true,
    // saveUninitialized: false,
    resave: false,
    // saveUninitialized: true,
    saveUninitialized: false,
    store,
  })
);

// aLoggerMiddleware
app.use((req, res, next) => {
  // console.log(store);
  console.log("Log bark: " + req.method + ", url: " + req.url);
  next();
});

//** set own cookie  */
// app.use((req, res, next) => {
//   const { headers: { cookie } } = req;
//   if (cookie) {
//       const values = cookie.split(';').reduce((res, item) => {
//           const [name, value] = item.trim().split('=');
//           return { ...res, [name]: value };
//       }, {});
//       res.locals.cookie = values;
//   }
//   else res.locals.cookie = {};
//   next();
// });

app.use(express.static("public"));
app.use("/users", usersRoute);
app.use("/auth", authRoute);
// app.use("/profile", profileRoute);
// const usr = '/users';
// console.log("---usersRoute: " + usr, usersRoute);

app.use(passport.initialize());
app.use(passport.session());

//** locals foe ejs? */
// app.use((req, res, next) => {
//   res.locals.user = req.user;
//   next();
// });

// app.get('/', async (req, res) => {
//     const results = await db.promise().query(`SELECT * FROM TEST`);
//     res.status(200).send(results[0][0]); //sendStatus;
// });

app.get("/pie", (req, res) => {
  db.query(
    "SELECT ticker, date_purchased, number, sharePrice FROM equity ORDER BY ticker ASC",
    function (err, rows) {
      if (err) throw err;
      var largemC = 0;
      var mediummC = 0;
      var smallmC = 0;

      var largepE = 0;
      var mediumpE = 0;
      var smallpE = 0;

      const data = rows.map((row) => row.number * row.sharePrice);
      const labels = rows.map((row) => row.ticker);
      var count = 0;
      var somme = 0;
      for (let i = 0; i < rows.length; i++) {
        const stockTicker = rows[i].ticker;
        var names = [];

        name(stockTicker, function (name) {
          names.push(name);
          somme += data[i];
          console.log("somme", somme);

          indicator1(stockTicker, function (mC, pE) {
            if (mC > 1000000000) {
              largemC += 1;
            } else if (mC > 200000000) {
              mediummC += 1;
            } else {
              smallmC++;
            }

            if (pE > 25) {
              largepE += 1;
            } else if (pE > 20) {
              mediumpE += 1;
            } else {
              smallpE++;
            }

            //this has to go inside indicator
            count++;
            if (count === rows.length) {
              const name2 = names.sort();

              res.render("firstPage.ejs", {
                data: data,
                labels: labels.sort(),
                names: name2,
                somme: somme,
                largemC: Math.round(largemC),
                mediummC: Math.round(mediummC),
                smallmC: Math.round(smallmC),
                largepE: Math.round(largepE),
                mediumpE:Math.round(mediumpE),
                smallpE: Math.round(smallpE),
                count: count,
              });
            }

            //indicator ends here
          });

          //name end here
        });
      }
    }
  );
});

app.get("/home", (req, res) => {
  db.query(
    "SELECT ticker, date_purchased, number, sharePrice FROM equity ORDER BY ticker ASC",
    function (err, rows) {
      if (err) throw err;
      const labels = rows.map((row) => row.ticker);
      const data = rows.map((row) => row.number * row.sharePrice);
      var somme = 0;
      var cyclical = 0;
      var sensitive = 0;
      var defensive = 0;
      var techcount = 0;
      var energycount = 0;
      var cstaple = 0;
      var finance = 0;
      var health = 0;
      var industrials = 0;
      var estate = 0;
      var count = 0;
      var returns = 0;

      for (let i = 0; i < rows.length; i++) {
        somme += data[i];
        const stockTicker = rows[i].ticker;
        const sharePrice = rows[i].sharePrice;
        const number = rows[i].number;
        var totalRetour = 0;

        stockSector(stockTicker, function (sector) {
          if (sector == "TECHNOLOGY") {
            techcount += 1;
            sensitive = sensitive + 1;
          } else if (sector == "LIFE SCIENCES") {
            health += 1;
            defensive += 1;
          } else if (sector == "FINANCE") {
            finance += 1;
            cyclical += 1;
          } else if (sector == "TRADE & SERVICES") {
            // consumer staples stocks
            cstaple += 1;
            defensive += 1;
          } else if (sector == "MANUFACTURING") {
            industrials += 1;
            sensitive += 1;
          } else if (sector == "ENERGY & TRANSPORTATION") {
            energycount += 1;
            sensitive += 1;
          } else if (sector == "REAL ESTATE & CONSTRUCTION") {
            cyclical += 1;
            estate += 1;
          }

          // name(stockTicker, function (name,price) {

          //   retour =  (price - sharePrice) * number;
          //   totalRetour += retour;

          count++;
          if (count === rows.length) {
            var persensitive = sensitive / count;
            var pertechcount = techcount / sensitive;
            var perenergycount = energycount / sensitive;
            var perindustrials = industrials / sensitive;

            var percyclical = cyclical / count;
            var perestate = estate / cyclical;
            var perfinance = finance / cyclical;

            var perdefensive = defensive / count;
            var perhealth = health / defensive;
            var percstaple = cstaple / defensive;

            var data2 = [];
            data2.push(cyclical, sensitive, defensive);

            res.render("secondPage.ejs", {
              labels: labels,
              data: data,
              somme: somme,
              data2: data2,

              persensitive: persensitive,
              perenergycount: perenergycount,
              pertechcount: pertechcount,
              perindustrials: perindustrials,

              percyclical: percyclical,
              perestate: perestate,
              perfinance: perfinance,

              perdefensive: perdefensive,
              perhealth: perhealth,
              percstaple: percstaple,
            });
          }
        });
      }
    }
  );
});

app.get("/add", (req, res) => {
  res.render("add");
});

app.post("/add", (req, res) => {
  var ticker = req.body.ticker;
  var sharePrice = req.body.share_price;
  var number = req.body.number;
  var datePurchased = req.body.purchased_date;

  console.log("ticker", ticker);
  console.log("sharePrice", sharePrice);
  console.log("number", number);
  console.log("datePurchased", datePurchased);

  db.query("SELECT COUNT (*) FROM equity", function (err, results) 
  {
    if (err) throw err;
    count = results[0].count
    if (count > 5) {
      res.send("Can't add more Equities");
    } else {
      var sql = `INSERT INTO equity (ticker, date_purchased, number, sharePrice) VALUES ('${ticker}', '${datePurchased}', '${number}', '${sharePrice}')`;
      db.query(sql, function (err) {
        console.log("1 record inserted");
        console.log("count is", count)
        res.redirect("home");
      });
    }
  });

  // var sql = `INSERT INTO equity (ticker, date_purchased, number, sharePrice) VALUES ('${ticker}', '${datePurchased}', '${number}', '${sharePrice}')`;
  // db.query(sql, function (err) {
  //   if (err) throw err;
  //   console.log("1 record inserted");
  //   res.redirect("/home");
  // });
});

function stockSector(symbol, callback) {
  var key1 = "D82RGKW7PC6AAW7D";
  var url = `https://www.alphavantage.co/query?function=OVERVIEW&symbol=${symbol}&apikey=${key1}`;
  request.get(
    {
      url: url,
      json: true,
      headers: { "User-Agent": "request" },
    },
    (err, res, data) => {
      if (err) {
        console.log("Error:", err);
        callback(null);
      } else if (res.statusCode !== 200) {
        console.log("Status:", res.statusCode);
        callback(null);
      } else {
        callback(data["Sector"], data["Name"]);
      }
    }
  );
}

function name(symbol, callback) {
  var key2 = "ZULUUL0G6E56XDBG";
  var url = `https://www.alphavantage.co/query?function=OVERVIEW&symbol=${symbol}&apikey=${key2}`;
  request.get(
    {
      url: url,
      json: true,
      headers: { "User-Agent": "request" },
    },
    (err, res, data) => {
      if (err) {
        console.log("Error:", err);
        callback(null);
      } else if (res.statusCode !== 200) {
        console.log("Status:", res.statusCode);
        callback(null);
      } else {
        callback(data["Name"], data["50DayMovingAverage"]);
      }
    }
  );
}

//Neddnew keys!
function indicator1(symbol, callback) {
  var key3 = "1HKKQ5A1H7QMX727";
  var url = `https://www.alphavantage.co/query?function=OVERVIEW&symbol=${symbol}&apikey=${key3}`;
  request.get(
    {
      url: url,
      json: true,
      headers: { "User-Agent": "request" },
    },
    (err, res, data) => {
      if (err) {
        console.log("Error:", err);
        callback(null);
      } else if (res.statusCode !== 200) {
        console.log("Status:", res.statusCode);
        callback(null);
      } else {
        callback(data["MarketCapitalization"], data["PERatio"]);
      }
    }
  );
}

// function CurrentReturns(symbol, callback) {
//   var key4 = "SSRYSSP0L2570WCB";
//   fetch(
//     `https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=${symbol}&interval=5min&apikey=${key4}`
//   )
//     .then((response) => response.json())
//     .then((json) => {
//       var data = json["Monthly Time Series"][Object.keys(data)[1]];
//       console.log("this is me ", data)
//       var data2 = json["Monthly Time Series"][Object.keys(data)[1]];
//       console.log("this is me ", data2)
//       // var days = Object.keys(data)[1];
//       // var price = data[days]["4. close"];
//       console.log("this is me ", data)
//       callback("hello")

//     });
// }

// app.get("/return", (req, res) => {
//     db.query(
//         "SELECT ticker, date_purchased, number, sharePrice FROM equity ORDER BY ticker ASC",
//         function (err, rows) {
//             if (err) throw err;

//             // // var trOe = 0;
//             // // var trOa = 0;
//             var count = 0;
//             var returns = 0;

//             for (let i = 0; i < rows.length; i++) {

//                 const stockTicker = rows[i].ticker;
//                 const date_purchased = rows[i].date_purchased;
//                 const sharePrice = rows[i].sharePrice;
//                 const number = rows[i].number;

//                 CurrentReturns(stockTicker, function (money) {

//                     var gains = (money - sharePrice) * number;
//                     returns += gains;
//                     console.log(returns)
//                     count++
//                     if (count === rows.length) {
//                         console.log("money value ");
//                         console.log(returns)
//                     }

//                 })

//             }

//         }
//     )
// });

app.listen(port, () => {
  console.log("Server humming on port: " + port);
});
