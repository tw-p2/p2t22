var COC = function(){
    var rental =  Number(document.getElementById('in-flow').value);
    var cost = Number(document.getElementById('out-flow').value);
    var result = (rental/cost) * 100;
    var elem = document.getElementById('result');
    elem.value = result.toString();
  }
  var clickButton = document.getElementById("coc");
  clickButton.addEventListener("click", COC);

  var ROI = function(){
    var ARI =  Number(document.getElementById('in-flow-1').value);
    var AOC =  Number(document.getElementById('in-flow-2').value);
    var MV = Number(document.getElementById('out-flow-1').value);
    var result = parseFloat(((ARI - AOC) / MV) * 100).toFixed(2);
    var elem = document.getElementById('result-1');
    elem.value = result.toString();
  }
  var clickButton = document.getElementById("roi-return");
  clickButton.addEventListener("click", ROI);

  var CAP = function(){
    var NOI =  Number(document.getElementById('in-flow-3').value);
    var PP = Number(document.getElementById('out-flow-2').value);
    var result = parseFloat((NOI / PP) * 100).toFixed(2);
    var elem = document.getElementById('result-2');
    elem.value = result.toString();
  }
  var clickButton = document.getElementById("cap-return");
  clickButton.addEventListener("click", CAP);