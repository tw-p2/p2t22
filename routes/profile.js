const { Router } = require('express');
const db = require('../DB_cnx');

const router = Router();

router.use((req, res, next) => {
    console.log('-Made req to /routes/profile');
    next();
});

router.post('/', 
  [check('fullName')
    .isLength({ min: 1 }).withMessage('1 character min for full name.'),
  check('email').notEmpty().withMessage('Email required.'),],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ errors: errors.array() });}
    const { fullName, email } = req.body;
    if (fullName && email) {
      try {
        db.promise().query(`INSERT INTO profile (fullName, email) VALUES ('${fullName}', '${email}')`);
        res.status(201).send({ msg: `-made profile: ${req.user.username}` });
      } catch (err) {
          console.log(err); }
      console.log(fullName, email);
  }
});


module.exports = router;
