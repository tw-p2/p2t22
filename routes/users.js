const { Router } = require('express');
// import db connection from its file
const db = require('../DB_cnx');
const { check, validationResult } = require('express-validator');

const router = Router();

router.use((req, res, next) => {
    console.log('-Made req to /routes/users');
    next();
});

// router.get('/', async (req, res) => {
//     const results = await db.promise().query(`SELECT * FROM TEST`);
//     res.status(200).send(results[0][0]); //sendStatus;
// });

// router.get('/folios', (req, res) =>  {
//     res.json({ route: 'Folios' });
// });


// router.get('/profile', function(req, res, next) {
//   var user = req.user;
//   res.render('profile', { title: 'profile', user: user });
// });

router.post('/', 
    [check('username')
      .isLength({ min: 1 }).withMessage('1 character min for username.')
      .notEmpty().withMessage('Username required.')
      .exists().withMessage('Username taken.'),
    check('email').notEmpty().withMessage('Email required.'),
    check('password').notEmpty().withMessage('Password required.'),],
    (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ errors: errors.array() });}
    const { username, email, password } = req.body;
    if (username && password && email) {
        try {
          db.promise().query(`INSERT INTO demo (username, password, email, date_created) VALUES 
            ('${username}', '${password}', '${email}', CURRENT_TIME())`);
          res.status(201).send({ msg: '-made user' });
        } catch (err) {
            console.log(err); }
        console.log(username, password);
    }
  });

//** just fullname, email  */  
// router.post('/profile', 
//   [check('fullName')
//     .isLength({ min: 1 }).withMessage('1 character min for full name.'),
//   check('email').notEmpty().withMessage('Email required.'),],
//   (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         res.status(400).json({ errors: errors.array() });}
//     const { fullName, email } = req.body;
//     if (fullName && email) {
//       try {
//         db.promise().query(`INSERT INTO profile (fullName, email) VALUES ('${fullName}', '${email}')`);
//         res.status(201).send({ msg: '-made profile:' });
//       } catch (err) {
//         console.log(err); }
//       console.log("req.user.fullname: " + req.user.fullName + ", " + fullName + ", " + email);
//   }
//   console.log("req.user.fullname: " + req.user.fullName + ", " + fullName + ", " + email);
// });

router.post('/profile', 
  [check('fullName')
    .isLength({ min: 1 }).withMessage('1 character min for full name.'),
    check('email').notEmpty().withMessage('Email required.'),
    check('phone').notEmpty().withMessage('phone required.'),
    check('urlLinked').notEmpty().withMessage('urlLinked required.'),
    check('street').notEmpty().withMessage('street required.'),
    check('city').notEmpty().withMessage('city'),
    check('state').notEmpty().withMessage('state required.'),
    check('zip').notEmpty().withMessage('zip required.'),],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ errors: errors.array() });}
    const { fullName, email, phone, urlLinked, street, city, state, zip } = req.body;
    if (fullName && email && phone && urlLinked && street && city && state && zip) {
      try {
        db.promise().query(`INSERT INTO profile (fullName, email, phone, urlLinked, street, city, state, zip) VALUES ('${fullName}', '${email}', '${phone}', '${urlLinked}', '${street}', '${city}', '${state}', '${zip}')`);
        res.status(201).send({ msg: '-made profile:' });
      } catch (err) {
        console.log(err); }
      console.log("req.user.fullname: " + req.user.fullName + ", " + fullName + ", " + email);
  }
});


module.exports = router;

/** session.id, connect.id is cookie */
//* post method: for every post request made to this endpoint (/users - from app)
//*  this endpoint (which is/users - from app)
//* this method will expect a request body w/ both u-name & pwd& are both truthy * /
























// router.post('/', (req, res) => {
//     const { username, password } = req.body;
//     if (username && password) {
//         console.log(username, password);
//         // try {
//         //     db.promise().query(`INSERT INTO users VALUES('${username}','${password}')`);
//         //     res.sendStatus(201).send({ msg: 'User Created' });
//         // } catch (err) {
//         //     console.log(err);
//         // }   
//     }
// });

// app.get("/login", (req, res) => {
//     // handle login page
//     res.sendFile("login.html");
//  });
 
//  app.post("/login", (req, res) => {
//     // check auth credentials from the login form
//     if (credentials good) {
//         req.session.authenticated = true;
//          res.redirect("/someOtherPage.html");
//     } else {
//         req.session.authenticated = false;
//         res.redirect("/login.html");
//     }
 
//  });
 
//  // middleware to allow access of already authenticated
//  app.use((req, res, next) => {
//     // check if session already authenticated
//     if (req.session.authenticated) {
//         next();
//     } else {
//         res.redirect("/login.html");
//     }
//  });
 
//  // route that relies on previous middleware to prove authentication
//  app.get("/somethingElse", (req, res) => {
//     // do something for this authenticated route
//  });



// next: Express router which, when invoked, xcutes midware succeeding current midware.




// router.get('/profile', (req, res) => {
//     res.json({ route: 'Profile'});
// });



