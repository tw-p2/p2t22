let search;
const lastUpdated = new Date().toLocaleString();

$(document).ready(function() {


  $("#search").on("click", function() {
    $(".searchbar").toggleClass("hidden");
  });

  $("#viewChart").on("click", function() {
    $("#stockChart").toggleClass("hidden");
  });

  $(".searchbar").keypress(function(e) {
    if(e.which == 13) {
      search = $(".searchbar").val();
      getStockInfo();
      getStockChart();
    }
  });
})

function getStockInfo() {
  data = $.ajax({
    	type: "GET",
    	url: `https://api.iextrading.com/1.0/stock/${search}/quote`,
    	dataType: "json",
    	success: function(data){
        let name = data.symbol;
        let price = rounder(data.latestPrice, 2);
        let change = data.changePercent;
        let closeTime = new Date(data.closeTime).toLocaleString();

        $(".header .name").text(name);
        $(".header .price").text(price);
        $(".header .change").text((rounder(change, 4) * 100).toFixed(2) + "%");
        $(".closeTime").text(`Trading Window Closes: ${closeTime}`);

        if(change >= 0) {
          $(".header .change").css("color", "green");
          $(".header .change").prepend("&#x25B2;");
        } else {
          $(".header .change").css("color", "red");
          $(".header .change").prepend("&#x25BC;");
        }
      }
  });
}

// Trading View Widget //

function getStockChart(){
  new TradingView.MediumWidget({
  "container_id": "tv-medium-widget-5e6f9",
  "symbols": [
    [
      "Google",
      search
    ]
  ],
  "greyText": "Quotes by",
  "gridLineColor": "#e9e9ea",
  "fontColor": "#83888D",
  "underLineColor": "#dbeffb",
  "trendLineColor": "#4bafe9",
  "width": "80%",
  "height": "400px",
  "locale": "en"
  });
}


function displayTime() {
  var x = new Date()
  var ampm = x.getHours( ) >= 12 ? ' PM' : ' AM';
  hours = x.getHours( ) % 12;
  hours = hours ? hours : 12;
  hours = hours.toString().length==1? 0 + hours.toString() : hours;
  
  var minutes = x.getMinutes().toString()
  minutes = minutes.length == 1 ? 0 + minutes : minutes;
  
  var seconds = x.getSeconds().toString()
  seconds = seconds.length == 1 ? 0 + seconds : seconds;
  
  var month = (x.getMonth() + 1).toString();
  month = month.length == 1 ? 0 + month : month;
  
  var dt = x.getDate().toString();
  dt = dt.length == 1 ? 0 + dt : dt;
  
  var x1 = month + "/" + dt + "/" + x.getFullYear(); 
  x1 = x1 + ", " +  hours + ":" +  minutes + ":" +  seconds + " " + ampm;
  document.getElementById('time').innerHTML = x1;
  update();
}

function update(){
  var refresh=1000; // Refresh rate in milli seconds //
  mytime=setTimeout('displayTime()',refresh)
}

update()

setInterval(getStockInfo, 10000);
