const { Router } = require('express');
const passport = require('passport');

const router = Router();

// ONLY THIS WORKS!!
router.get('/logout', function(req, res) {
  req.session.destroy(function() {
    res.clearCookie('connect.sid');
    res.redirect('/');
  });
});
// req.session.destroy(function() {
//   res.clearCookie('connect.sid');
//   res.redirect('/');
// });

// router.get('/logout', function (req, res, next) {
//   req.logout(function(err) {
//     if (err) { return next(err); }
//     res.sendStatus(200).redirect("/");
//   });
  
// });

// router.get('/logout', function (req, res) {
//   console.log("-you rang logout GET in auth.js?")
//   // console.log("-you rang logout GET in auth.js: id, username= " + req.user.id +" , " + req.user.username);
//   req.session.destroy(function (err) {
//     res.sendStatus(200).clearCookie('connect.sid').redirect("/"); ;
//     // res.sendStatus(200).redirect("/"); 
//     // res.end; 
//   });
// });

router.post('/logout', function (req, res, next) {
  console.log("-you rang logout POST in auth.js?")
  req.logout(function(err) {
    if (err) { return next(err); }
    res.redirect('/');
  });
});



router.post('/login', passport.authenticate('local'), (req, res) => {
    res.sendStatus(200);
});

module.exports = router;
