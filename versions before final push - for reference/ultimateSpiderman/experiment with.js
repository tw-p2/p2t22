app.get("/indi", (req, res) => {
    con.query(
      "SELECT ticker, date_purchased, number, sharePrice FROM equities ORDER BY ticker ASC",
      function (err, rows) {
        if (err) throw err;
        var largemC = 0;
        var mediummC = 0;
        var smallmC = 0;
  
        var largepE = 0;
        var mediumpE= 0;
        var smallpE = 0;
  
        // // var trOe = 0;
        // // var trOa = 0;
        var count = 0;
  
        for (let i = 0; i < rows.length; i++) {
  
          const stockTicker = rows[i].ticker;
   
  
        indicator(stockTicker, function (mC, pE) {
          
          if (mC  > 1000000000){
            largemC += 1;
          } else if (mC > 200000000){
            mediummC += 1;
          } else{
            smallmC++;
          }
  
          if (pE  > 25){
            largepE += 1;
          } else if (pE > 20){
            mediumpE += 1;
          } else{
            smallpE++;
          }
  
          count++
          if (count === rows.length) {
            console.log("mc value")
            console.log(largemC,mediummC,smallmC ),
            console.log("pE value")
            console.log(largepE,mediumpE,smallpE )
    
          }
  
  
        })
  
  
      
      }
      
  
    }
      )})
  
  
  
  
  
  app.get("/indi2", (req, res) => {
        con.query(
          "SELECT ticker, date_purchased, number, sharePrice FROM equities ORDER BY ticker ASC",
          function (err, rows) {
          
      
            var tpBr = 0;
            var tpEr = 0;
            var tpSr = 0;
            var trOe = 0;
            var trOa = 0;
  
            var count = 0;
      
            for (let i = 0; i < rows.length; i++) {
      
              const stockTicker = rows[i].ticker;
           
      indicator(stockTicker, function (pBr,pEr,pSr,rOe ,rOa ) {
                tpBr += pBr
                tpEr += pEr
                tpSr += pSr
                trOe += rOe
                trOa += rOa
      
              count++;
  
              if (count === rows.length) { 
  
              console.log(
                "total tpBr ",
                tpBr,
                "total tpEr ",
                tpEr,
                "total tpSr ",
                tpSr,
                "total trOe ",
                trOe ,
                "total trOa ",
                trOa )
            }
        
               }
               
               )
               }
      
      
            })
      
      
          
          })
          
    
          app.get("/return", (req, res) => {
            con.query(
              "SELECT ticker, date_purchased, number, sharePrice FROM equities ORDER BY ticker ASC",
              function (err, rows) {
                if (err) throw err;
               
          
                // // var trOe = 0;
                // // var trOa = 0;
                var count = 0;
                var returns = 0;
          
                for (let i = 0; i < rows.length; i++) {
          
                  const stockTicker = rows[i].ticker;
                  const date_purchased = rows[i].date_purchased;
                  const sharePrice = rows[i].sharePrice;
                  const number = rows[i].number;
           
          
                CurrentReturns(stockTicker, function (money) {
          
                  var gains =  (money - sharePrice) * number ;
                  returns += gains;
                  console.log(returns)
                  count++
                  if (count === rows.length) {
                    console.log("money value ");
                    console.log(returns)
                  }
          
                  })
          
              
              }
              
          
            }
              )})
              
              


              //Neddnew keys!
function indicator(symbol, callback) {
    //var key1 = "21R8ADVVPNXQCF6G";
    var key2 = "1HKKQ5A1H7QMX727";
    //var key3 = "54YQGS18OP1OZQ41";
    var url = `https://www.alphavantage.co/query?function=OVERVIEW&symbol=${symbol}&apikey=${key2}`;
    request.get(
      {
        url: url,
        json: true,
        headers: { "User-Agent": "request" },
      },
      (err, res, data) => {
        if (err) {
          console.log("Error:", err);
          callback(null);
        } else if (res.statusCode !== 200) {
          console.log("Status:", res.statusCode);
          callback(null);
        } else {
          callback(data["PriceToBookRatio"],data["PERatio"],data["PriceToSalesRatioTTM"],data["ReturnOnEquityTTM"],data["ReturnOnAssetsTTM"] );
        }
      }
    );
  }
  
  function CurrentReturns(symbol, callback) {
    var key4 = "SSRYSSP0L2570WCB";
    fetch(
      `https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=${symbol}&interval=5min&apikey=${key4}`
    )
      .then((response) => response.json())
      .then((json) => {
        var data = json["Monthly Time Series"];
        var days = Object.keys(data)[1];
        var price = data[days]["4. close"];
        console.log("from function", price)
        callback(price)
  
     
      });
  }


  function getStockInfo(symbol, callback) {
    data = {
        type: "GET",
        url: `https://api.iextrading.com/1.0/stock/${symbol}/quote`,
        dataType: "json",
        success: function(data){
          let price = rounder(data.latestPrice, 2);
          callback(price); 
        }
    };
  }  