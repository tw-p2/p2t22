<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- mobile metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1">
        <!-- site metas -->
        <title>Understand the Stock Market - World Financial</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content=""> 
        <!-- bootstrap css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/news.css">
        <!-- favicon -->
        <link rel="icon" type="image/svg+xml" href="images/favicon.svg" />
      </head>
<body>
        <!-- Navigation-->
        <div class="header_section header_bg">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="index.html">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="about.html">About</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="news.html">Educate</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="services.html">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="team.html">Team</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="contact.html">Contact Us</a>
                  </li>
                </ul>
              </div>
              <div class="profile-text"><a href="sign-up.html">Sign in</a></div>
            </nav>
        </div>
        <!-- Post Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <h2 class="section-heading">The Stock Market</h2>
                        <p>Over the short-term, stocks and other securities can be battered or bought by any number of fast market-changing events, making the stock market behavior difficult to predict. Emotions can drive prices up and down, people are generally not as rational as they think, and the reasons for buying and selling are generally accepted.</p>
                        <p>Behaviorists argue that investors often behave irrationally when making investment decisions thereby incorrectly pricing securities, which causes market inefficiencies, which, in turn, are opportunities to make money. However, the whole notion of EMH is that these non-rational reactions to information cancel out, leaving the prices of stocks rationally determined.</p>
                        <p>A stock market crash is often defined as a sharp dip in share prices of stocks listed on the stock exchanges. In parallel with various economic factors, a reason for stock market crashes is also due to panic and investing public's loss of confidence. Often, stock market crashes end speculative economic bubbles.</p>
                        <h2 class="section-heading">Market Crashes</h2>
                        <p>There have been famous stock market crashes that have ended in the loss of billions of dollars and wealth destruction on a massive scale. An increasing number of people are involved in the stock market, especially since the social security and retirement plans are being increasingly privatized and linked to stocks and bonds and other elements of the market. There have been a number of famous stock market crashes like the Wall Street Crash of 1929, the stock market crash of 1973–4, the Black Monday of 1987, the Dot-com bubble of 2000, and the Stock Market Crash of 2008.</p>
                        <p>One of the most famous stock market crashes started October 24, 1929, on Black Thursday. The Dow Jones Industrial Average lost 50% during this stock market crash. It was the beginning of the Great Depression.</p>
                        <blockquote class="blockquote">I will tell you how to become rich. Close the doors. Be fearful when others are greedy. Be greedy when others are fearful. <br> <span class="highlight">— Warren Buffett</span> </blockquote>
                        <p>By the end of October, stock markets in Hong Kong had fallen 45.5%, Australia 41.8%, Spain 31%, the United Kingdom 26.4%, the United States 22.68%, and Canada 22.5%. Black Monday itself was the largest one-day percentage decline in stock market history – the Dow Jones fell by 22.6% in a day. The names "Black Monday" and "Black Tuesday" are also used for October 28–29, 1929, which followed Terrible Thursday—the starting day of the stock market crash in 1929.</p>
                        <p>The 2020 stock market crash was a major and sudden global stock market crash that began on 20 February 2020 and ended on 7 April. This market crash was due to the sudden outbreak of the global pandemic, COVID-19. The crash ended with a new deal that had a positive impact on the market.</p>
                        <h2 class="section-heading">Margin Investing</h2>
                        <p>Stock that a trader does not actually own may be traded using short selling; margin buying may be used to purchase stock with borrowed funds; or derivatives may be used to control large blocks of stocks for a much smaller amount of money than would be required by outright purchase or sales.</p>
                        <img class="img-fluid" src="images/stock-market-g40b311e90_1280.jpg" alt="..." />
                        <span class="caption text-muted">Essential Rules of <span class="highlight">Personal Finance</span> : Separate Your Emotions from Your Finances.</span>
                        <p>In margin buying, the trader borrows money (at interest) to buy a stock and hopes for it to rise. Most industrialized countries have regulations that require that if the borrowing is based on collateral from other stocks the trader owns outright, it can be a maximum of a certain percentage of those other stocks' value. In the United States, the margin requirements have been 50% for many years (that is, if you want to make a $1000 investment, you need to put up $500, and there is often a maintenance margin below the $500).</p>
                        <p>A margin call is made if the total value of the investor's account cannot support the loss of the trade. (Upon a decline in the value of the margined securities additional funds may be required to maintain the account's equity, and with or without notice the margined security or any others within the account may be sold by the brokerage to protect its loan position. The investor is responsible for any shortfall following such forced sales.)</p>
                       <!-- Divider-->
                       <hr class="my-4" />
                       <p>
                           Text generated from
                           <a href="https://www.boom-online.co.uk/">Boom</a>
                           &middot; Image by <a href="https://pixabay.com/users/sergeitokmakov-3426571/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=6531146">Sergei Tokmakov, Esq. https://Terms.Law</a> from <a href="https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=6531146">Pixabay</a>
                       </p>
                    </div>
                </div>
            </div>
        </article>
        <!--footer section start -->
        <div class="footer_section layout_padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <h4 class="about_text">About World Financial</h4>
                        <div class="location_text"><img src="images/map-icon.png"><span class="padding_left_15">Locations</span>
                        </div>
                        <div class="location_text"><img src="images/call-icon.png"><span class="padding_left_15">+01
                                9876543210</span></div>
                        <div class="location_text"><img src="images/mail-icon.png"><span
                                class="padding_left_15">support@team22.com</span></div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h4 class="about_text">About World Financial</h4>
                        <p class="dolor_text">Established in 2023, <br> helping individuals manage funds and investments, <br>
                            put your money to work</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h4 class="about_text">LinkedIn</h4>
                        <div class="footer_images">
                            <div class="footer_images_left">
                                <div class="image_12"><img src="images/img-12.png"></div>
                                <div class="image_12"><img src="images/img-12.png"></div>
                            </div>
                            <div class="footer_images_right">
                                <div class="image_12"><img src="images/img-12.png"></div>
                                <div class="image_12"><img src="images/img-12.png"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h4 class="about_text">Newsletter</h4>
                        <input type="text" class="mail_text" placeholder="Enter your email" name="Enter your email">
                        <div class="subscribe_bt"><a href="#">Subscribe</a></div>
                        <div class="footer_social_icon">
                            <ul>
                                <li><a href="#"><img src="images/fb-icon1.png"></a></li>
                                <li><a href="#"><img src="images/twitter-icon1.png"></a></li>
                                <li><a href="#"><img src="images/linkedin-icon1.png"></a></li>
                                <li><a href="#"><img src="images/youtub-icon1.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- copyright section start -->
                <div class="copyright_section">
                    <div class="copyright_text">&copy; 2023 <a href="index.html">Team 22</a>. All Right Reserved.</div>
                </div>
                <!-- copyright section end -->
            </div>
        </div>
        <!--footer section end -->
        <!-- Javascript files-->

</body>
</html>
